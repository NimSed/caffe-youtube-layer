import sys, os
#sys.path.insert(0, os.environ['CAFFE_ROOT']+'/python')

import caffe
import numpy as np
import random
import multiprocessing
from multiprocessing import Process
from multiprocessing import Queue as mpQueue
import time
import matplotlib.pyplot as plt
from skimage.transform import rescale
from skimage.util import crop

from dl_manager import dl_manager

class NimaYoutubeDataLayer: #(caffe.Layer):

    def setup(self,bottom,top):

        #--- Initialize the random generator based on time
        from datetime import datetime
        random.seed(datetime.now())

        #--- read parameters from `self.param_str`
        self.params = eval(self.param_str)

        #--- Create the dl_manager object and start it
        self.Q = mpQueue(200) #this is the Q in which we'll receive frame sequences
        self.dl_manager = dl_manager(self.Q,16);
        self.dl_manager.Start("car",5,3,2,5)
        
        #-- we also have an internal iteration counter, just in case
        self.internal_iter_count = 0;
                                              
    def reshape(self,bottom,top):

        # no "bottom"s for input layer
        if len(bottom)>0:
            raise Exception('cannot have bottoms for input layer')

        # make sure you have the right number of "top"s
        if len(top) != 3:
            raise Exception('Wrong number of tops!')

        for i in range(len(top)):
            top[i].reshape(self.params["batch_size"],1,self.params["h"],self.params["w"])
    
    def forward(self,bottom,top):


        batch_size = self.params["batch_size"];
        W = self.params["w"]
        H = self.params["h"]

        for i in range(batch_size):
            imgs = self.Q.get()

            #--- resize/crop calculations for this sequence
            #  -> preserves aspect ratio
            h,w,_ = imgs[0].shape

            if w < W or h < H: #if img has at least one dimension smaller than the target size
                #-upscale the image to have both dimensions >= desired size
                scale = max([W/w,H/h]);
            else:
                scale = 1
            
            ws = np.round(w*scale)
            hs = np.round(h*scale)

            #- now crop the image (+ a simple translational augmentation)
            cw = random.randint(0,ws-W)
            ch = random.randint(0,hs-H)
            cw2 = ws-W-cw
            ch2 = hs-H-ch

            #--- and fill in the output
            for j in range(len(top)):
                img = imgs[j];

                if scale != 1:
                    img = rescale(img,scale);
                img = crop( img,( (ch,ch2),(cw,cw2),(0,0) ) )
                
                #top[j].data[i,...] = img
                print('forward:',img.shape)

                plt.subplot(batch_size,3,3*i+j+1)
                plt.imshow(img)
        
        plt.show(block=True);

        # update the internal iteration counter
        self.internal_iter_count += 1;
        
        
    def backward(self, top, propagate_down, bottom):
        pass


#========= Tester
if __name__ == '__main__':

    param_str = ('{' 
    '"query":"car",'
    '"batch_size": 8,'
    '"w":1280,'
    '"h":384,'
    '}')

    layer = NimaYoutubeDataLayer();
    layer.param_str = param_str;
    layer.setup(None,None);
    
    time.sleep(10);
    while True:
        layer.forward(None,[1,1,1])
