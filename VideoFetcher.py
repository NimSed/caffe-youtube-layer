import youtube_dl
import os
import multiprocessing
from multiprocessing import Process, Lock, Value
from multiprocessing import Queue as mpQueue
from queue import Queue 
from ctypes import c_char
import numpy as np
import cv2
import glob
import imageio
import random
import numpy as np
import sys
    
def loadVideoFrames(videoObject,indices):

    frames = [];

    for i in indices:
        frames.append(videoObject.get_data(i)) #this line may throw exceptions

    return frames

def getRandSequenceOfFrames(videoObject,seqLength,samplingRate=1):

    vidLength = len(videoObject)

    #--- Obtain the random indices
    startFrame = random.randint(0,vidLength-1-(seqLength-1)*framesDistance)
    frames = list(range(startFrame,startFrame+(seqLength-1)*framesDistance+1,framesDistance))

    #--- extract the frames
    seq = loadVideoFrames(videoObject,frames)
    return seq

def sampleSeqsWholeVideoUniform(videoObject,destList,nSequences,seqLength,framesDistance=1):

    vidLength = len(videoObject)

    nSequences = min(nSequences,vidLength//seqLength)

    for startFrame in (np.floor(np.linspace(0,vidLength-seqLength*framesDistance-1,nSequences))).astype('int'):
        indices = list(range(startFrame,startFrame+(seqLength-1)*framesDistance+1,framesDistance))
        try:
            seq = loadVideoFrames(videoObject,indices)
            destList.append(seq)
        except: #some times the estimated length of the video is apparently wrong, causing failed frame extractions
            pass

        print('Extracted these frames ',indices, 'from', videoObject._filename)
        
def download_youtube_video_single(vid):

    #-- create a temp file name automatically
    pid = os.getpid()
    
    temp_folder = '/tmp/'
    ydl_opts = {
        'outtmpl': temp_folder+'/%d-'%pid+'%(id)s',
        'quiet': '1',
        'no_warnings': '1',
    }
    ydl = youtube_dl.YoutubeDL(ydl_opts)
    ydl.download(['https://www.youtube.com/watch?v=%s'%vid])
    
    #-- load it back to memory and delete the temp folder
    temp_filename = glob.glob(temp_folder+'/%d-%s*'%(pid,vid))
    temp_filename = temp_filename[0]


    return temp_filename
    
    
def download_vid_get_sequences_whole_vid(VidID,destList,nSequences,seqLength,framesDistance):

    #--- Download the video
    temp_filename = download_youtube_video_single(VidID);
    
    #--- Fetch the required sequences
    #- and put them in the destQ
    vid = imageio.get_reader(temp_filename,'ffmpeg')
    sampleSeqsWholeVideoUniform(vid,destList,nSequences,seqLength,framesDistance)

    #--- Delete the temp file
    os.remove(temp_filename)

def vid_get_sequences(vid,destList,startIndex,nSequences,nSeqFrames,framesDistance,seqsDistance):
    '''
    Note that here I don't care about the video length and rely on exceptions
    Note that seqsDistance can start from 1, which represents tight overlapped sequences.
    However, it probably is better to not have overlaps, for it seemingly slows down frame extraction
    '''
    #print('Seq-set start index:',startIndex)

    seqStartFrame = startIndex
    while True:
        indices = list(range(seqStartFrame,seqStartFrame+(nSeqFrames-1)*framesDistance+1,framesDistance))
        try:
            seq = loadVideoFrames(vid,indices)
            destList.append(seq)
        except: #probably we have reached the end of the video
            break

        #print('Extracted these frames ',indices, 'from', vid._filename)

        seqStartFrame += seqsDistance #This should increment before the following break, for the return value to be correct

        #- Let's stop if we have enough sequences
        if len(destList) == nSequences:
            break;
    
    return seqStartFrame

def check_pid(pid):        
    """ Check For existence of a unix pid. """
    try:
        os.kill(pid, 0)
    except OSError:
        return False
    else:
        return True

def exit_if_no_parent(initial_parent_id):
    #-- check if parent is still alive
    ppid = os.getppid();
    if ppid != initial_parent_id :
        sys.exit('generator: parent changed => should commit suicide!')
    elif not check_pid(ppid):
        sys.exit('generator: parent not running any more!')        

def forever_loop(vidIDQ,listQ,maxSeqPerList,nSeqFrames,framesDistance,seqsDistance,shouldStop,initial_parent_id):
    ''' This function is always spawned as a new sub-process '''

    pid = os.getpid()
    while True:

        #-- wait until there is at least a new video ID in the Q
        #print(pid,'Fetching new vidID');
        
        while True:
            if(shouldStop.value):
                sys.exit()
            
            try:
                ID = vidIDQ.get_nowait()
                break
            except:
                exit_if_no_parent(initial_parent_id)
                
        #print(pid,'DONE: ',ID);

        #--- Start getting sequence-sets from the video ID, chunk by chunk until there's no more left
        NextStartIndex = 0
        while True:

            #-- Download the video and connect to it!
            try:
                temp_filename = download_youtube_video_single(ID);
                vid = imageio.get_reader(temp_filename,'ffmpeg')
            except: #if for any reason the video cannot be downloaded, this daemon should not die
                print('Failed to download ',ID)
                break 
            
            #-- create a local list as a container for the sequence set
            l = []
        
            #-- fetch a constant number of sequences from the downloaded video
            NextStartIndex = vid_get_sequences(vid,l,NextStartIndex,maxSeqPerList,
                                               nSeqFrames,framesDistance,seqsDistance)

            #-- Append the fetched Q to the QQ (this should wait for an empty slot)
            if(len(l) > 0):
                #print(pid,'adding to listQ');
                while True:
                    if(shouldStop.value):
                        sys.exit()

                    try:
                        listQ.put(l)
                        break
                    except:
                        exit_if_no_parent(initial_parent_id)
                #print(pid,'DONE');

            #-- Check if this has been the last possible seq-set
            if len(l) < maxSeqPerList:
                #- Remove the temp file
                os.remove(temp_filename)

                #- break the loop to go on with the next video ID
                break

class VideoFetcher:

    def __init__(self):

        #--- Initialize the necessary shared variables
        self.listQ = mpQueue(2); #this will be used as a Q of length 2, of lists of sequences
        self.vidIDQ = mpQueue(3)
        self.shouldStop = Value('i',False)

        
    def addVidIDToDownload(self,ID):
        self.vidIDQ.put_nowait(ID) #may throw exception (in case the list is already full)
        
    def start(self,maxSeqPerBuffer,seqLength,framesDistance,seqsDistance):
        #--- Initialize the current local list
        self.localList = []

        #--- Start the forever_loop
        self.process = Process(target = forever_loop,
                               args=(self.vidIDQ,self.listQ,maxSeqPerBuffer,seqLength,framesDistance,seqsDistance,
                                     self.shouldStop,os.getpid()))
        self.process.start()

    def popSingleRandomFrameSeq(self): #throws an exception if there's no available sequence
        if len(self.localList) == 0:
            self.localList = self.listQ.get_nowait(); #may throw exception

        ind = random.randint(0,len(self.localList)-1);
        return self.localList.pop(ind);
        
    def stop(self):
        if(self.process):
            self.shouldStop.value = True


#========= Tester
if __name__ == '__main__':
    fetcher = VideoFetcher()

    fetcher.start(10,3,2,5)
    import time
    time.sleep(5)
    fetcher.addVidIDToDownload('YQHsXMglC9A')
    time.sleep(15)
    fetcher.addVidIDToDownload('sTP9djp5O14')
    time.sleep(20)
    fetcher.stop()
    #filename = download_youtube_video_single('sTP9djp5O14')
