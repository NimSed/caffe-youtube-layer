import multiprocessing
from multiprocessing import Process, Lock
from multiprocessing import Queue as mpQueue
import random
from random import shuffle,randint

from VideoFetcher import VideoFetcher

class dl_manager:

    def __init__(self,Q,numFetchers):
        self.Q = Q
        self.numFetchers = numFetchers

    def GetListOfVidsSinglePage(self,query,page):
        ''' 
        The following block is inspired by the code by Jason Brooks from:
        https://stackoverflow.com/questions/29069444/returning-the-urls-as-a-list-from-a-youtube-search-query
        '''
        from urllib.parse import quote
        from urllib.request import urlopen
        from bs4 import BeautifulSoup
        
        textToSearch = query
        query = quote(textToSearch)
        url = "https://www.youtube.com/results?search_query=" + query + '&page=%d'%page
        response = urlopen(url)
        html = response.read()
        soup = BeautifulSoup(html,"html5lib")

        ids = [];
        for vid in soup.findAll(attrs={'class':'yt-uix-tile-link'}):
            spl = vid['href'].split('?v=');

            if(len(spl) == 2): #otherwise it is probably a non-video link
                ids.append(spl[1]);

        return ids

    def GetListOfVids(self,query):

        #--- for now use the cached list!
        ids = ['YQHsXMglC9A', 'ih2xubMaZWI', 'PDZcqBgCS74', 'RlopJFHgzbY', 'kK42LZqO0wA', 'stRg7XmWWV4', 'DxiCH6RDfx0', '_-I74oGGL5I', 'MdK_VAMsXfE', 'cosC1OA9Q0g', '2TU7dE_9Vi4', 'ifjj9DfAVkU', 'vlZ9kjCrGJw', 'VYz2oMx7Psc', 'xU2seCIIJC8', 'rDWuqrJAyGw', '_t1gfn9aqiQ', 'VC3zgH-61-U', 'LtQUJMBH8uE', 'bnVUHWCynig', 'YQHsXMglC9A', 'ih2xubMaZWI', 'PDZcqBgCS74', 'RlopJFHgzbY', 'kK42LZqO0wA', 'stRg7XmWWV4', 'DxiCH6RDfx0', '_-I74oGGL5I', 'MdK_VAMsXfE', 'cosC1OA9Q0g', '2TU7dE_9Vi4', 'ifjj9DfAVkU', 'vlZ9kjCrGJw', 'VYz2oMx7Psc', 'xU2seCIIJC8', 'rDWuqrJAyGw', '_t1gfn9aqiQ', 'VC3zgH-61-U', 'LtQUJMBH8uE', 'bnVUHWCynig', 'UBYnT8JY7sE', '8mxBYx_z5hg', '6biv8AQcjNU', '8DIyw-NbYwQ', 'MlhXLOLB-Cw', 'DfG6VKnjrVw', 'Ih61MJ72v1Y', '4K-LS-fj21A', 'gQtPtgkMTq4', 'OmnDEUD9NyI', 'Dhz4QA97oEo', 'PjYWZaNtZ_Y', '5ohh11-N9CE', '74mz0asOTVI', 'Mj4ZmF5taY4', '9y3N-ko3g3U', '9h0Arg_-380', 'NwU3EaMYEUI', 'weeVDca5ZOI', 'M8Fj9GNa5nc', '0_I0DBUA_GI', '6TyW8hRBzzU', 'YgxCW7h_fp0', '8xGpT_NOnLI', 'Ez1u9auDNqQ', 'XCrq4iR7l6Q', 'Z1B9E0JCy6Q', 'i6nwu7GS3RM', '1cG6y6cgaFI', 'MWacs-rFiQY', 'Ueovfepan-o', 'rblYSKz_VnI', 'esKj1dltF7g', 'cgu1BKAnJFA', '2R5vIXU3c-k', 'tfYc00htbkY', 'eiNbJWrRsTg', '8o4Zj98FeX4', 'GGWH0cChgms', 'gn6hzArzafY', '8Z02tv-wbBs', 'ziaf3RrG80U', 'EXDRox1Bkro', 'FB4OOpo4sBU', 'Fs60CC_wcbE', 'U9D9I_FosMM', 'monIw3TrI14', '9Y_9HCms4E8', 'Am9YuY_s_Eo', 'YmxW3NGa-kk', 'Tm-uAFGOAm8', 'vih3Jmwhdqo', 'sjkHtr98stc', 'v88AXpqSX9g', 'mKRgRwttnmc', 'U8nibIN7i6M', 'tvsFofB6MxE', 'lcA0SVkNtH0', 't-Z2BDDogCI', 'zxd2ujf98T0', 'ZD3Hj6QNluw', '-6savx6aWys', '3BOnkec0BWw', 'hIcJxJL4MOM', 'S7Q-hx7dV7E', 'ZOUiwYK-PH0', '84RxK4N1wfE', 'scaA1B9YpeE', 'TNlqK4JnjFc', '6giqHss4_LI', 'vCwQZkIfjTM', 'phGur4M9ics', 'cPiNCrkJZHg', '2wnS5-UGrEg', 'q5wgbIz2aLQ', '5XUjR-6UqPA', 'qqLtPbEmbJs', '9wVqop7pR8o', 'UAMyh8DjCrQ', '1JLmdfPzBiY', 'vUIFKziKa6k', 'JE3ZzrhTzzE', 'zxIE85c3RO4', 'Ou1jX5VthIY', '-gG-T4udopM', 'BalUcQpYN6k', 't4OfBez9V38', 'AlyKQZlmJUg', 'PPQNbTPb-F0', 'xozOFqXR-2Q', '-TjeW4RTq4g', 'KXGqHYNTNHU', 'vnVOAHy8u6Q', '1WWVqxsmcM0', '0YLsj1bIOY4', 'fuOXgX3n290', '62XB9IbMnxQ', 'BjrHOQctkrM', 'lW1FOVl1rR4', 'tVlcKp3bWH8', 'tVlcKp3bWH8', 'HpvBW6ei2E8', 'jun4rWJ_L6I', 'cU-O39hALss', 'skZxb5sBoiU', 'MAqopkr3hRc', '9mOMmP_aKso', 'LiaYDPRedWQ', 'sGO-vvAeIw0', 'ML7GOLeTV28', 'faNNSTzw-Lw', 'JH8gvhl4rH8', 'AiC7ZX5K9L4', 'TYT0ooxF150', 'CaHWq1rheeU', 'RKRq89yJivM', 'DePBqtkK56M', '7leQIdK9ucw', 'YtSPQIK15uc', '6eRlpHXX5_U', 'ItDi5qYxMU0', 'gvsA8yvhSnw', 'q10KoruX2Fs', '5GQnC6UUsZw', '02IcmAmZKSc', '3ywIu30lqKA', 'uuaaizVZxio', '46OD9DGI7a0', 'mv326-zVpAQ', 'Vsu7k4DmrVQ', '_761BgWD6jo', '6I2RGEkFIFc', '4ErL2T2RIxI', 'jgDzuKPhPPc', 'di088wlqgg0', 'hcpbLBDFUXU', 'wGRtuMy87pE', '1qLe3V8qmN8', 'uYHITNpdgkc', 'JpGID4yvFpY', '7ZkejDqTuSM', '7HF1Sfos3v4', 'Bf-5l6GlqyQ', 'm3Jdn-y9Z5E', 'ackdK5bbpMU', 'zG2TEQvNwJU', '-bIEwHxiGlA', 'tWs1E2BfNZE', '52KmnKb0XJI', 'XUKyO0ndVYA', 'QNcU0e-EwXM', 'ptypuNgfj58', 'ePoi0_zSnYk', 'b_B0cSWHmyE', 'wJnBTPUQS5A', 'fN1Cyr0ZK9M', 'KN1DJTQZwX0', 'lh-IhHU-agg', 'KN1DJTQZwX0', 'Xvs5El-Rurg', '4W6QaAUE_-g', 'To_eXsapXCA', 'PUjvaMWKeBI', 'ffk4rot0XF4', 'OuVZLvGPSiU', 'SAVgJQVo2oE', 'oBov7-NPAbg', 'bqEQTzNbL-I', 'D7c7rBv-FUU', 'z-Ozc2fje9A', 'PzSZdAGDGYE', 'iETuUCq5pXc', 'xXJSmGfw9oI', 'ckLqfX62u_I', 'cgN94MpClZ8', 'gqtlr2cMyiE', 'q21CZRd-_s0', 'LWgqWuJG5Jg', 'ptypuNgfj58', 'b_B0cSWHmyE', 'ePoi0_zSnYk', 'lh-IhHU-agg', 'XRgiNUtJurk', 'IZNlCoZFaoE', 'wHKBEQh3EyY', 'EguL9IWo6yU', 'WHMfxoTQ96Y', '4W6QaAUE_-g', 'vmp-xmguqh4', 'ZIBrkoAN0yk', 'EIeuGsaYO4w', 'fN1Cyr0ZK9M', 'xXJSmGfw9oI', 'Uef0Qr56mKo', 'l7N2wssse14', '8UEE7lUpvzI', '0Ysr_uOE1Uc', '5Pt03oU6RHI', 'rKNwx82kPjY', 'vmp-xmguqh4', 'gghDRJVxFxU', 'MOysl6rVBdM', 'VYp4BypEirg', '8OSGZqIxwAQ', '8UEE7lUpvzI', 'WZjFMj7OHTw', 'XRgiNUtJurk', 'WHMfxoTQ96Y', 'g-FDuNq7JzE', 'KOG46EHXKkw', 'Zox2qo-wzb0', 'ZIBrkoAN0yk', 'D25e1el8N1w', 'zy5K8ApSzhI', '3xdfBwFb2DU', 'YxrMNT4SX2E', 'rOU4YiuaxAM', 'jK5FOyMhDOY', 'QfifaWeiem4', 'kACinK6edys', 'lQ6-nCLeDsI', 'S-VDbbjIto4', 'ycl3vl98MMM', 'B4v2jZAkroA', 'aeQlnlMpizI', 'eut-ehTyUJs', 'OciO2MPnmzU', 'EIeuGsaYO4w', 'lMf2NN0M1oM', 'BmRtyiSlqiA', 'ZuJWQzjfU3o', '8f1z-nHvt3c', 'Uef0Qr56mKo', 'tZkDSxhsqzg', 'IZNlCoZFaoE', 'YP5DDw-HX7E', 'l7N2wssse14', 'NmxaFf-lpZY', 'x23rTDl4AMs', 'jALFdV9lLl8', 'rHf6b8LUYCw', '7-iLzd-HKRg', 'OmH4fPWtGA0', '443G-yp4pTg', 'BM7Jkwqzdts', 'emG3YhU9Efg', 'QNDlwHW92OY', 'R7VBUDL7PCk', 'CSqVcTcQ_pc', 'mFrghyAyNTg', 'RfwGkplB_sY', 'HMSWAUAKJn0', 'aRWo7qMWxf8', '5qi8hhiYEQA', 'al2DFQEZl4M', '-evuKPEp3KE', '9jjiWS__Mp0', 'e2zyjbH9zzA', '_Zmr2JmAwkU', 'FJ85Hep0kD0', 'lLeCB7Kn-VE', 'J69oCCM1EcI', '6KtgMrkkIjE', 'kXctQ0CHKOo', 'wT8NO5FDS7E', 'ZgMAAzzQz7s', 'itGNQbJwRSk', 'GMi4MtyDg40', 'UAA8vnYvz6Y', 'XwMLa2tX-ug', 'zLkCWT2neuI', 'ajW0Oa13gAk', 'OKkLV1zE8M0', '98dai6CC5BA', 'NuBtuzOuk7M', 'W9NuPlqekO4', 'EcXrxwCh9Zo', 'Iy7RwBwDiD8', 'enu-qR0H_uk', 'xRxzOVftQP8', '6yR0sx9Y24o', '5Pt03oU6RHI', 'e6Tfq1hfkP0', '-aL2I--TCHM', 'eV71mpbvl-g', 'op7ImVx20BI', 'zm4XGByZLdc', 'QjHncnLpV5M', 't0eHcbez_LU', 'c0QAv3staJs', '3r5lnQYwdbE', 'w7zeMtUK0SY', 'rwhvFLHIlBs', 'y3FI02OO_kU', '5fM61urj1FQ', 'Og1QRtcWdEY', 'H-YmQZFI1lk', 'hSDfBkkWLSU', 'zImez8m2kXw', 'HdTBpKf1fsI', 'HdTBpKf1fsI', 'jsezr0qiFIc', 'ZyyuV-gkavg', 'uDxDW9jEjHg', '8VmRUPPQyds', 'IRW2O4QZhgs', 'pHFcIod7yis', '2Yq3JuKY2kQ', 'TqFiWKt18gw', 'ja3XCa8GcRk', '8Dm4GhaYj84', 'kmfeKUNDDYs', 'dOy7vPwEtCw', 'v63TYgNtOV0', 'Qx8vdxXJn1c', 'AuchYZ_YnXQ', 'OC7IZryyoq4', 'zQkg0DGI2OA', 'zQkg0DGI2OA', 'cKxRvEZd3Mw', 'zX09WnGU6ZY', 'ro6I4Mlu1Ak', 'CbBGSvgkowk', '4cXW_MzRyvQ', '3eyyZBAImdI', 'v9kTVZiJ3Uc', 'NUOKzwMbmfk', 'jXJN3UdilgY', 'WH9sgoeJQJA', 'YbSYTRnDwUM', '4UFLC5U6y2g', '6r5fVBbzW3M', '9Pq5Agm7MWE', 'hw3TmDkNOVI', 'Cs5vNABd_1M', 'PMbNqkxTae0', '8iAwbKBVK2w', 'k2vx8MaQqUo', 'byJ8OevF2Lo', 'PMg6PnBXIdg', 'BQPIt4NkC7c', 'BQPIt4NkC7c', '6ywCNN1gFXY', 'irUTeIkdbbU', 'yhujgY6m0RQ']
        return ids
        
        #--- Loop over page numbers (max 1000 pages, which practically is never reached)
        ids = [];
        for page_number in range(1000):
            page_ids = self.GetListOfVidsSinglePage(query,page_number)

            #- Let's keep the first returned list of ids, to determine when we should stop
            if page_number == 0:
                ids0 = page_ids;


            #- Check if we should stop
            if page_number > 0:
                n = len(page_ids)
                ni = len(set(ids0).intersection(page_ids));
                
                print(n,ni,len(ids))

                if n == 0:
                    break

            #- append the retrieved urls to the main list
            ids.extend(page_ids);


        return ids

    def forever_loop(self,Q,query,maxSeqPerBuffer,seqLength,framesDistance,seqsDistance):

        #--- Get list of videos from youtube
        VideoIDs = self.GetListOfVids(query);

        #--- Create multiple video fetchers
        fetchers = [];
        for i in range(self.numFetchers):
            fetchers.append(VideoFetcher())

        #--- Assign the intial video IDs to fetchers
        for fetcher in fetchers:
            VidIndex = randint(0,len(VideoIDs))
            fetcher.addVidIDToDownload(VideoIDs[VidIndex])
            
        #--- Start fetchers (each fetcher spawns a new process internally)
        for fetcher in fetchers:
            fetcher.start(maxSeqPerBuffer,seqLength,framesDistance,seqsDistance)

        #--- The forever loop
        while True:

            #--- Check for ready out buffers
            #-- Get a shuffled list of fetchers
            perm = list(range(self.numFetchers))
            shuffle(perm)

            #-- Loop on the fetchers
            for i in perm:
                fetcher = fetchers[i]
                
                # Fetch a single rabdom sequence and put it in the global Q
                try:
                    seq = fetcher.popSingleRandomFrameSeq()
                    Q.put(seq)
                    #print('dl_manager: fetched a sequence. Q Size:',Q.qsize());
                    
                    
                except: #nothing to fetch from this video downloader
                    # Assign to the fetcher, the next video id to fetch (or fail if the ID Q is already full)
                    try:
                        nextVidIndex = randint(0,len(VideoIDs))
                        fetcher.addVidIDToDownload(VideoIDs[nextVidIndex])
                    except:
                        pass

                    import time
                    time.sleep(0.5)

    def Start(self,query,maxSeqPerBuffer,seqLength,framesDistance,seqsDistance):
        
        self.process = Process(target=self.forever_loop,
                               args=(self.Q,query,maxSeqPerBuffer,seqLength,framesDistance,seqsDistance))
        self.process.start()
        
        
    def Stop(self):
        pass


    
#========= Tester
if __name__ == '__main__':

    mp_manager = multiprocessing.Manager()
    Q = mp_manager.Queue()

    dlm = dl_manager(Q,4)

    dlm.Start('hello',15,3,1,3)
